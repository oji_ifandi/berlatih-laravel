<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function daftar()
    {
        return view('halaman.daftar');
    }

    public function kirim(Request $request)
    {
        $nama = $request['nama'];
        $alamat = $request['address'];

        return view('halaman.home', compact('nama', 'alamat'));
    }
}
