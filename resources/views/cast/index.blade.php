@extends('layout.master')

@section('judul')
Halaman List Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-2">Tambah Data Cast</a>
<table class="table">
    <thead class="thead-light">
        <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Biodata</th>
        <th scope="col">Aksi</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                    <form action="/cast/{{$value->id}}" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$value->id}}" class="btn btn-info">Detail</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <input type="submit" class="btn btn-danger" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>

@endsection