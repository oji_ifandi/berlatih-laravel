<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@dashboard');
Route::get('/pendaftaran', 'BiodataController@daftar');
Route::post('/kirim', 'BiodataController@kirim');

// Route::get('/data-table', function () {
//     return view('halaman.data-table');
// });
// Route::get('/table', function () {
//     return view('halaman.table');
// });



// CRUD CAST

//CREATE
// form input data create cast
route::get('/cast/create', 'CastController@create');
// untuk menyimpan data ke databse
route::post('/cast', 'CastController@store');

//READ
// untuk membaca data di database
route::get('/cast', 'CastController@index');
// untuk menampilkan data berdasar kategori
route::get('/cast/{cast_id}', 'CastController@show');

//UPDATE
// form edit data berdasarkan id cast
route::get('/cast/{cast_id}/edit', 'CastController@edit');
// untuk update data berdasarkan id cast
route::put('/cast/{cast_id}', 'CastController@update');

//DELETE
route::delete('/cast/{cast_id}', 'CastController@destroy');